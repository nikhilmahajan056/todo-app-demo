import { useWeb3Modal } from "@web3modal/react";
import { useState, useEffect } from "react";
import { useAccount, useDisconnect } from "wagmi";
import getConfig from "next/config";
import Web3 from "web3";
import styles from "./Dashboard.module.css";

const { publicRuntimeConfig } = getConfig();

export default function Dashboard() {
  const [loading, setLoading] = useState(false);
  const { open } = useWeb3Modal();
  const { address, isConnected } = useAccount();
  const { disconnect } = useDisconnect();
  const [tasks, setTasks] = useState([]);
  const [addTaskClicked, setAddTaskClicked] = useState(false);
  const [assignTaskClicked, setAssignTaskClicked] = useState(false);

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_CONTRACT_OWNER.toLowerCase() == address.toLowerCase()) getAllTasks();
    if (process.env.NEXT_PUBLIC_CONTRACT_OWNER.toLowerCase() != address.toLowerCase()) getTasks();
  }, [address])

  async function onOpen() {
    setLoading(true);
    await open();
    setLoading(false);
  }

  function onClick() {
    if (isConnected) {
      disconnect();
    } else {
      onOpen();
    }
  }

  function handleClosePopup() {
    setAddTaskClicked(false);
    setAssignTaskClicked(false);
  }

  // Create Task Popup component
const CreateTaskPopup = ({ onClose }) => {

  const [taskName, setTaskName] = useState("");
  const [taskDescription, setTaskDescription] = useState("");

  async function createTask() {
    if (taskName != "" && taskDescription != "") {
      try {
        setLoading(true);
        const web3 = new Web3(window.ethereum);
        const smartContract = new web3.eth.Contract(publicRuntimeConfig.contractAbi, process.env.NEXT_PUBLIC_CONTRACT_ADDRESS);
        const resp = await smartContract.methods.createTask(taskName, taskDescription).send({from: address});
  
        if (resp.status) {
          setLoading(false);
          await getAllTasks();
          onClose();
        } else {
          throw new Error(await resp.json())
        }
      } catch (error) {
        console.error(error);
        setLoading(false);
        onClose();
      }
    }
  }

  return (
    <div className={styles.popup}>
      <div className={styles.popupContent}>
        <h2>Add new task</h2>
        <form>
          <label htmlFor="taskName">Task name:</label>
          <input
            type="text"
            id="taskName"
            value={taskName}
            onChange={(e) => setTaskName(e.target.value)}
          />

          <label htmlFor="taskDescription">Task description:</label>
          <input
            type="text"
            id="taskDescription"
            value={taskDescription}
            onChange={(e) => setTaskDescription(e.target.value)}
          />
        </form>
        <div className={styles.popupButtons}>
          <button className="dashboardButton" onClick={createTask} disabled={loading}>Save</button>
          <button className="dashboardButton" onClick={(e) => {e.preventDefault(); setTaskName(""); setTaskDescription(""); onClose();}} disabled={loading}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

// Assign Task Popup
const AssignTaskPopup = ({ task, onClose }) => {
  const [userAddress, setUserAddress] = useState("");

  async function assignTask() {
    if (userAddress != "") {
      try {
        setLoading(true);
        const web3 = new Web3(window.ethereum);
        const smartContract = new web3.eth.Contract(publicRuntimeConfig.contractAbi, process.env.NEXT_PUBLIC_CONTRACT_ADDRESS);
        const resp = await smartContract.methods.assignTask(task.taskId, userAddress).send({from: address});
  
        if (resp.status) {
          setLoading(false);
          await getAllTasks();
          onClose();
        } else {
          throw new Error(await resp.json())
        }
      } catch (error) {
        console.error(error);
        setLoading(false);
        onClose();
      }
    }
  }

  return (
    <div className={styles.popup}>
      <div className={styles.popupContent}>
        <h2>Assign task</h2>
        <form>
          <label htmlFor="taskName">Task name:</label>
          <input
            type="text"
            id="taskName"
            value={task.taskName}
            disabled
          />

          <label htmlFor="taskDescription">Task description:</label>
          <input
            type="text"
            id="taskDescription"
            value={task.taskDescription}
            disabled
          />

          <label htmlFor="taskDescription">User address:</label>
          <input
            type="text"
            id="taskDescription"
            value={userAddress}
            onChange={(e) => setUserAddress(e.target.value)}
          />
        </form>
        <div className={styles.popupButtons}>
          <button className="dashboardButton" onClick={assignTask} disabled={loading}>Save</button>
          <button className="dashboardButton" onClick={(e) => {e.preventDefault(); onClose();}} disabled={loading}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

// Mark task as completed
  async function markCompleted(taskId) {
      try {
        setLoading(true);
        const web3 = new Web3(window.ethereum);
        const smartContract = new web3.eth.Contract(publicRuntimeConfig.contractAbi, process.env.NEXT_PUBLIC_CONTRACT_ADDRESS);
        const resp = await smartContract.methods.markTaskAsCompleted(taskId).send({from: address});
  
        if (resp.status) {
          setLoading(false);
          await getTasks();
          handleClosePopup();
        } else {
          throw new Error(await resp.json())
        }
      } catch (error) {
        console.error(error);
        setLoading(false);
        handleClosePopup();
      }
  }

// Fetch all the tasks assigned to user
  async function getTasks() {
    try {
      const web3 = new Web3(window.ethereum);
      const smartContract = new web3.eth.Contract(publicRuntimeConfig.contractAbi, process.env.NEXT_PUBLIC_CONTRACT_ADDRESS);
      const resp = await smartContract.methods.getTasksList().call({from: address});
      if (resp) {
        setTasks(resp);
      } else {
        throw new Error(await resp)
      }
    } catch (error) {
      console.error(error)
    }
  }

// Fetch all the tasks created by owner. Only contract owner can invoke this function
  async function getAllTasks() {
    try {
      const web3 = new Web3(window.ethereum);
      const smartContract = new web3.eth.Contract(publicRuntimeConfig.contractAbi, process.env.NEXT_PUBLIC_CONTRACT_ADDRESS);
      const resp = await smartContract.methods.getAllTasks().call({from: address});
      if (resp.length) {
        setTasks(resp);
      } else {
        throw new Error(resp)
      }
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <div className={styles.mainSection}>
        <div className={styles.heading}>
            <h1 className={styles.header}>To Do Application</h1>
            <div className={styles.buttons}>
                { (address.toLowerCase() == process.env.NEXT_PUBLIC_CONTRACT_OWNER.toLowerCase()) && <button className={styles.headingButton} style={{marginRight: "1vw"}} onClick={(e) => {e.preventDefault(); setAddTaskClicked(true);}} disabled={loading}><b>+</b> Create Task</button>}
                <button className={styles.headingButton} onClick={onClick} disabled={loading}>Disconnect</button>
            </div>
        </div>
        <div className={styles.taskSection}>
            {
              tasks.length > 0 ? (
                <>
                    {/* <div className={styles.taskCardUpper}> */}
                    {
                      tasks.map((task) => {
                        return (
                          <>
                            <div className={styles.taskCard}>
                            <h2 style={{display:"block", textAlign: "left"}}>#{task.taskId.toString()}</h2>
                            <h2 style={{display:"block", textAlign: "right"}}>{task.taskName}</h2>
                            <p className={styles.taskCardDescription}>{task.taskDescription}</p>
                            {
                              address.toLowerCase() == process.env.NEXT_PUBLIC_CONTRACT_OWNER.toLowerCase() ? (
                                <>
                                  {task.isAssigned ? <div className={styles.taskCompleted}>&#x2713;</div> : <button className={styles.taskCardButton} onClick={(e) => {e.preventDefault(); setAssignTaskClicked(task);}} disabled={loading}>Assign</button>}
                                </>
                              ) : (
                                <>
                                  {task.isCompleted ? <div className={styles.taskCompleted}>&#x2713;</div> : <button className={styles.taskCardButton} onClick={(e) => {e.preventDefault(); markCompleted(task.taskId);}} disabled={loading}>Complete</button>}   
                                </>
                              )
                            }
                            {
                              assignTaskClicked && (
                                <AssignTaskPopup task={assignTaskClicked} onClose={handleClosePopup}/>
                              )
                            }
                            </div>
                          </>
                        )
                      })
                    }
                    {/* </div> */}
                </>
              ) : (
                <div>
                    <h3>No tasks available at this moment!</h3>
                </div>   
              )
            }
        </div>
        {
          addTaskClicked && (
            <CreateTaskPopup onClose={handleClosePopup}/>
          )
        }
    </div>
  );
}
