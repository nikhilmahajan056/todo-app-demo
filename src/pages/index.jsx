import { Web3Button } from "@web3modal/react";
import Dashboard from "../components/Dashboard";
import { useAccount } from "wagmi";

export default function HomePage() {
  const { isConnected } = useAccount();

  return (
    <>
      {/* Predefined button  */}
      {!isConnected && <Web3Button icon="show" label="Connect Wallet" balance="show" /> }
      {isConnected && <Dashboard /> }
    </>
  );
}
