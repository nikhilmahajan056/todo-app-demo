// SPDX-License-Identifier: MIT

pragma solidity ^0.8.18;

import "@openzeppelin/contracts/access/Ownable.sol";

contract TaskContract is Ownable {

    uint256 public id;

    struct Task {
        uint256 taskId;
        string taskName;
        string taskDescription;
        bool isCompleted;
        bool isAssigned;
    }

    Task[] public tasks;
    mapping(address => uint256[]) private tasksList;

    event TaskCreated(uint256 taskId, string taskName, string taskDescription);
    event TaskCompleted(uint256 taskId, bool isCompleted);

    /** 
     * @dev Create a new task.
     * @param name name of task
     * @param description add task description here
     */
    function createTask(string memory name, string memory description) public onlyOwner {
        uint256 taskId = id;
        tasks.push(Task({
            taskId: id,
            taskName: name,
            taskDescription: description,
            isCompleted: false,
            isAssigned: false
        }));
        id++;
        emit TaskCreated(taskId, name, description);
    }

    /** 
     * @dev Assign task to user. Only contract owner can assign task to user
     * @param taskId ID of the task created
     * @param userAddress wallet address of user
     */
    function assignTask(uint256 taskId, address userAddress) public onlyOwner {
        require(!tasks[taskId].isAssigned, "Task already assigned!");

        tasksList[userAddress].push(taskId);
        tasks[taskId].isAssigned = true;
    }

    /** 
     * @dev Checks if user is valid to mark the task as completed.
     * @param taskId ID of the task created
     * @param userAddress wallet address of user
     */
    function checkValidUser(uint256 taskId, address userAddress) internal view returns(bool) {
        for(uint256 i = 0; i < tasksList[userAddress].length; i++) {
            if (taskId == tasksList[userAddress][i]) return true;
        }

        return false;
    }

    /** 
     * @dev When assigned task to user, user can mark the task as completed
     * @param taskId ID of the task created
     */
    function markTaskAsCompleted(uint256 taskId) public {
        require(checkValidUser(taskId, msg.sender), "Invalid user!");
        require(!tasks[taskId].isCompleted, "Task already marked completed!");

        tasks[taskId].isCompleted = true;
        emit TaskCompleted(taskId, true);
    }

    /** 
     * @dev List all the tasks. Only contract owner can view all tasks
     * @return list of tasks
     */
    function getAllTasks() public onlyOwner view returns(Task[] memory) {
        Task[] memory intTasks = new Task[](id);
        for(uint256 i = 0; i < id; i++) {
            Task storage task = tasks[i];
            intTasks[i] = task;
        }
        return intTasks;
    }

    /** 
     * @dev Get task list for users. Users can view the assigned task
     * @return list of tasks assigned to user
     */
    function getTasksList() public view returns(Task[] memory) {
        Task[] memory intTasks = new Task[](tasksList[msg.sender].length);
        for(uint256 i = 0; i < tasksList[msg.sender].length; i++) {
            Task storage task = tasks[tasksList[msg.sender][i]];
            intTasks[i] = task;
        }
        return intTasks;
    }
}