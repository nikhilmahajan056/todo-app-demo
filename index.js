const { Web3, HttpProvider } = require("web3");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const contract = require("./config/ToDoContract.json");
require("dotenv").config()

const app = express();
const corseOptions = {
    "Access-Control-Allow-Origin" : "*",
    "Access-Control-Allow-Methods" : "OPTIONS, GET, POST",
    "Access-Control-Max-Age" : 200,
    "Access-Control-Allow-Headers" : "Content-Type"
}

app.use(cors(corseOptions));
app.use(bodyParser.json());

const PORT = process.env.PORT || 4000;

var provider = new HttpProvider(process.env.RPC_URL);
const web3 = new Web3(provider);
const ownerAccount = web3.eth.accounts.privateKeyToAccount(process.env.CONTRACT_OWNER_PRIVATE_KEY);
const smartContract = new web3.eth.Contract(contract.abi, process.env.CONTRACT_ADDRESS);

app.post("/createTask", async function(req, res) {
    try {
        const { taskName, taskDescription } = req.body;
        const transaction = {
            gas: 300000,
            gasPrice: await web3.eth.getGasPrice(),
            from: ownerAccount.address,
            to: process.env.CONTRACT_ADDRESS,
            data: smartContract.methods.createTask(taskName, taskDescription).encodeABI()
        }

        const signedTx = await web3.eth.accounts.signTransaction(transaction, process.env.CONTRACT_OWNER_PRIVATE_KEY);
        console.log("signedTx:", signedTx);
        var response = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        console.log("response:", response);

        res.status(200).send(`Transaction successful! More details: ${process.env.EXPLORER_LINK}/tx/${response.transactionHash}`);
    } catch (error) {
        console.error(error);
        res.status(500).send("Something went wrong!")
    }
});

app.post("/assignTask", async function (req, res) {
    try {
        const { taskId, assigneeAddress } = req.body;
        console.log(taskId, assigneeAddress);
        const transaction = {
            gas: 300000,
            gasPrice: await web3.eth.getGasPrice(),
            from: ownerAccount.address,
            to: process.env.CONTRACT_ADDRESS,
            data: smartContract.methods.assignTask(taskId, assigneeAddress).encodeABI()
        }

        const signedTx = await web3.eth.accounts.signTransaction(transaction, process.env.CONTRACT_OWNER_PRIVATE_KEY);
        console.log("signedTx:", signedTx);
        var response = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        console.log("response:", response);

        res.status(200).send(`Transaction successful! More details: ${process.env.EXPLORER_LINK}/tx/${response.transactionHash}`);
    } catch (error) {
        console.error(error);
        res.status(500).send("Something went wrong!")
    }
});

app.post("/completeTask", async function (req, res) {
    try {
        const { taskId } = req.body;
        console.log(taskId);
        const transaction = {
            gas: 300000,
            gasPrice: await web3.eth.getGasPrice(),
            from: ownerAccount.address,
            to: process.env.CONTRACT_ADDRESS,
            data: smartContract.methods.markTaskAsCompleted(taskId).encodeABI()
        }

        const signedTx = await web3.eth.accounts.signTransaction(transaction, process.env.CONTRACT_OWNER_PRIVATE_KEY);
        console.log("signedTx:", signedTx);
        var response = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        console.log("response:", response);

        res.status(200).send(`Transaction successful! More details: ${process.env.EXPLORER_LINK}/tx/${response.transactionHash}`);
    } catch (error) {
        console.error(error);
        res.status(500).send("Something went wrong!")
    }
});

app.listen(PORT, () => {
    console.log(`ToDo app listening on port ${PORT}`)
})