const TaskContract = artifacts.require("TaskContract");

var taskContractInstance;

contract("TaskContract", function (accounts) {
  it("should assert true", async function () {
    taskContractInstance = await TaskContract.deployed();
    return assert.isTrue(true);
  });

  it("should create task", async function () {
    await taskContractInstance.createTask("Task name 0", "Task description 0");
    const task = await taskContractInstance.tasks.call(0);
    return assert.isNotEmpty(task);
  })

  it("should assign task", async function () {
    await taskContractInstance.assignTask(0, accounts[1]);
    const task = await taskContractInstance.tasks.call(0);
    return assert.isTrue(task.isAssigned);
  })

  it("should mark task as completed", async function () {
    await taskContractInstance.markTaskAsCompleted(0, { from : accounts[1] });
    const task = await taskContractInstance.tasks.call(0);
    return assert.isTrue(task.isCompleted);
  })
});
