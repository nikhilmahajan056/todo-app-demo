module.exports = {
    output: 'standalone',
    publicRuntimeConfig: {
      // Will be available on both server and client
      contractAbi: [
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "taskId",
                    "type": "uint256"
                },
                {
                    "internalType": "address",
                    "name": "userAddress",
                    "type": "address"
                }
            ],
            "name": "assignTask",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "string",
                    "name": "name",
                    "type": "string"
                },
                {
                    "internalType": "string",
                    "name": "description",
                    "type": "string"
                }
            ],
            "name": "createTask",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "taskId",
                    "type": "uint256"
                }
            ],
            "name": "markTaskAsCompleted",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "previousOwner",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "newOwner",
                    "type": "address"
                }
            ],
            "name": "OwnershipTransferred",
            "type": "event"
        },
        {
            "inputs": [],
            "name": "renounceOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "taskId",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "bool",
                    "name": "isCompleted",
                    "type": "bool"
                }
            ],
            "name": "TaskCompleted",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "taskId",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "string",
                    "name": "taskName",
                    "type": "string"
                },
                {
                    "indexed": false,
                    "internalType": "string",
                    "name": "taskDescription",
                    "type": "string"
                }
            ],
            "name": "TaskCreated",
            "type": "event"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "newOwner",
                    "type": "address"
                }
            ],
            "name": "transferOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "getAllTasks",
            "outputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "taskId",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "taskName",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "taskDescription",
                            "type": "string"
                        },
                        {
                            "internalType": "bool",
                            "name": "isCompleted",
                            "type": "bool"
                        },
                        {
                            "internalType": "bool",
                            "name": "isAssigned",
                            "type": "bool"
                        }
                    ],
                    "internalType": "struct TaskContract.Task[]",
                    "name": "",
                    "type": "tuple[]"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "getTasksList",
            "outputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "taskId",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "taskName",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "taskDescription",
                            "type": "string"
                        },
                        {
                            "internalType": "bool",
                            "name": "isCompleted",
                            "type": "bool"
                        },
                        {
                            "internalType": "bool",
                            "name": "isAssigned",
                            "type": "bool"
                        }
                    ],
                    "internalType": "struct TaskContract.Task[]",
                    "name": "",
                    "type": "tuple[]"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "id",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "owner",
            "outputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        }
      ],
    }
}