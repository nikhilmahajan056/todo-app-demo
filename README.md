## IMPORTANT: Please install metamask wallet extension on your browser. Connect to Polygon Mumbai Testnet 

# To Do App

The example shows how to connect to Web3 wallets; and to get the user wallet info and create tasks, assign task and complete task. 

## Requirements
1. Node.Js: v18.14.2
1. Docker
1. Docker Compose
1. Ganache-cli
1. Truffle Suite

## You can self-host this application with support for all features using Node.js or Docker. Please find the details below for both the methods:

The smart contract is currently deployed on Polygon Mumbai Testnet. To test current smart contract, use below passphrase to import wallet account on Metamask. Import the wallet and switch to Polygon Mumbai Testnet.

> Wallet Passphrase: `nice bundle yellow festival scene tank depart smart clog edit resist release`


### Method 1: Using Node.js Server

Clone the repository and execute below command: 

```bash
cd todo-app-demo
```


.env.local.example file contains all the predefined environment variables to run the application. Rename .env.local.example file to .env.local.

If you want to use the predefined variables follow further steps. And if you want to add personalized environment variables check Optional section and skip the below steps

Execute below commands in the sequential manner: 

```bash
npm install

npm run dev
```

---
#### Optional (Can be skipped): Required only when using personalized environment variables

If you want to change the environment variables or use presonalized variables, follow below steps and add all the required variables.


1. Get your projectId at <u>https://cloud.walletconnect.com</u> and replace with NEXT_PUBLIC_PROJECT_ID
1. Replace smart contract address with NEXT_PUBLIC_CONTRACT_ADDRESS
1. Add smart contract owner address for NEXT_PUBLIC_CONTRACT_OWNER
1. Replace smart contract address with CONTRACT_ADDRESS
1. Add smart contract owner private key for CONTRACT_OWNER_PRIVATE_KEY
1. Add RPC URL for RPC_URL
1. Explorer link to check the details of the transaction for EXPLORER_LINK


After adding all the required variables in .env.local, execute below commands in the sequential manner: 

```bash
npm install

npm run dev
```
---


### Method 2: Using Docker Compose

Clone the repository and execute below command: 

```bash
cd todo-app-demo
```

1. [Install Docker Compose](https://docs.docker.com/compose/install/) on your machine, if not installed.
1. Host your containers: `docker compose up -d`.
1. Test smart contracts: `docker compose exec truffle truffle test`.

You can view your images created with `docker images`.
Open `http://localhost:3000` in your browser for the web application